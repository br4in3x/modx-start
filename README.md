# README #

This project is created for speed improvement of web-development with MODx Revolution and pdoTools.
In every new project I have to create same tvs, chunks, lines of code that common for each site. This repository contains the snapshot of MODx Revo with several settings already included. All you have to do is install it, change paths in config files and run it. Very big thanks to creators of MODx, Vasiliy Bezumkin and other MODx contributors for such great content management system and aswesome extensions!

### What already made ###

* Google Map setting. Just enter address in System Setting named "Site Addres", clear cache and use the html.map chunk
* AjaxForm for quick feedback already been made in chunk html.form
* New File Source for images, files, etc. (many of users uploading them whereever they want, that's not good). Already set for TV "common_image"
* Set of parameters for pdoResources snippet that ease snippet calling (include common_tv, disable tv_prefix and sort by menuindex ascending)
* Snippet phoneValidator that can be used with ajaxForm and validates correct format of phone (11 numbers without whitespaces or "+" sign)
* For page that represents list of resources already been made chunk with pdoMenu call in ajax mode (scroll down) pagination
* Robots.txt and Sitemap.xml created as Resources, everything handled by MODx (sitemap works throught pdoSitemap snippet)
* 404 error page has been made
* With installed yTranslit you can generate beatiful aliases. Nothing to do, api key is already set, use it for free

### Installed extensions ###

* dateago - for nice date formatting, try [[dateAgo?input=`[[+publishedon]]`]]
* tinymcerte - new verion of popular tinymce. Clean & nice
* ace - code editor
* minifyx - minifies all css and js, see https://rtfm.modx.com/extras/revo/minifyx
* ytranslit - for pretty translated aliases
* phpthumbof - adaptaition of phpthumb for MODx
* collections - special resource type for blog 
* bannery - using for sliders, banners, etc
* pdotools - bery nice tool to query almost anything. see http://docs.modx.pro/components/pdotools/snippets/
* formit - dependence of ajaxform
* ajaxmanager - speed up Manager
* ajaxform - nice ajax formit calls, see http://docs.modx.pro/components/ajaxform

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact