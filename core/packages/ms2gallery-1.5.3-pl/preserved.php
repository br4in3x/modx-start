<?php return array (
  'abe71ec35aa4b31d46e2c929401b9f2e' => 
  array (
    'criteria' => 
    array (
      'name' => 'ms2gallery',
    ),
    'object' => 
    array (
      'name' => 'ms2gallery',
      'path' => '{core_path}components/ms2gallery/',
      'assets_path' => '',
    ),
  ),
  'ce43be2461a3b5ccbe1f9c6df5822800' => 
  array (
    'criteria' => 
    array (
      'key' => 'ms2gallery_source_default',
    ),
    'object' => 
    array (
      'key' => 'ms2gallery_source_default',
      'value' => '2',
      'xtype' => 'modx-combo-source',
      'namespace' => 'ms2gallery',
      'area' => 'ms2gallery_resource',
      'editedon' => '2016-08-10 15:58:14',
    ),
  ),
  '03e7152c31f2970bf04f9efc0d4448c0' => 
  array (
    'criteria' => 
    array (
      'key' => 'ms2gallery_date_format',
    ),
    'object' => 
    array (
      'key' => 'ms2gallery_date_format',
      'value' => '%d.%m.%y %H:%M',
      'xtype' => 'textfield',
      'namespace' => 'ms2gallery',
      'area' => 'ms2gallery_resource',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '7b5d4f033e55719b90fef8fe45f52943' => 
  array (
    'criteria' => 
    array (
      'key' => 'ms2gallery_page_size',
    ),
    'object' => 
    array (
      'key' => 'ms2gallery_page_size',
      'value' => '20',
      'xtype' => 'textfield',
      'namespace' => 'ms2gallery',
      'area' => 'ms2gallery_resource',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '68427fdc1ea213f829f9fe63a9d1958f' => 
  array (
    'criteria' => 
    array (
      'key' => 'ms2gallery_set_placeholders',
    ),
    'object' => 
    array (
      'key' => 'ms2gallery_set_placeholders',
      'value' => '',
      'xtype' => 'combo-boolean',
      'namespace' => 'ms2gallery',
      'area' => 'ms2gallery_frontend',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'e3880b92b86b0613fa5b98d0d64c51e2' => 
  array (
    'criteria' => 
    array (
      'key' => 'ms2gallery_placeholders_prefix',
    ),
    'object' => 
    array (
      'key' => 'ms2gallery_placeholders_prefix',
      'value' => 'ms2g.',
      'xtype' => 'textfield',
      'namespace' => 'ms2gallery',
      'area' => 'ms2gallery_frontend',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '65a6038567f4cd8d49735580f086eeca' => 
  array (
    'criteria' => 
    array (
      'category' => 'ms2Gallery',
    ),
    'object' => 
    array (
      'id' => 19,
      'parent' => 0,
      'category' => 'ms2Gallery',
      'rank' => 0,
    ),
  ),
  '573ce4f185269200840c9e33144947ef' => 
  array (
    'criteria' => 
    array (
      'name' => 'tpl.ms2Gallery.row',
    ),
    'object' => 
    array (
      'id' => 23,
      'source' => 1,
      'property_preprocess' => 0,
      'name' => 'tpl.ms2Gallery.row',
      'description' => '',
      'editor_type' => 0,
      'category' => 19,
      'cache_type' => 0,
      'snippet' => '<div class="span2 col-md-2">
	<a href="[[+360x270:default=`[[+url]]`]]" class="thumbnail" data-image="[[+url]]">
		<img src="[[+120x90]]" alt="" title="[[+name]]" width="120" height="90">
	</a>
</div>',
      'locked' => 0,
      'properties' => NULL,
      'static' => 0,
      'static_file' => 'core/components/ms2gallery/elements/chunks/chunk.ms2gallery_row.tpl',
      'content' => '<div class="span2 col-md-2">
	<a href="[[+360x270:default=`[[+url]]`]]" class="thumbnail" data-image="[[+url]]">
		<img src="[[+120x90]]" alt="" title="[[+name]]" width="120" height="90">
	</a>
</div>',
    ),
  ),
  '9c4b1ec27dc1be264df6be64d488a30e' => 
  array (
    'criteria' => 
    array (
      'name' => 'tpl.ms2Gallery.outer',
    ),
    'object' => 
    array (
      'id' => 24,
      'source' => 1,
      'property_preprocess' => 0,
      'name' => 'tpl.ms2Gallery.outer',
      'description' => '',
      'editor_type' => 0,
      'category' => 19,
      'cache_type' => 0,
      'snippet' => '<div id="msGallery">
	<a rel="fancybox" href="[[+url:default=`/assets/ms2gallery/minishop2/img/web/ms2_big.png`]]" target="_blank">
		<img src="[[+360x270:default=`/assets/components/ms2gallery/img/web/ms2_medium.png`]]" width="360" height="270" alt="" title="" id="mainImage" />
	</a>
	<div class="row">
		[[+rows]]
	</div>
</div>',
      'locked' => 0,
      'properties' => NULL,
      'static' => 0,
      'static_file' => 'core/components/ms2gallery/elements/chunks/chunk.ms2gallery_outer.tpl',
      'content' => '<div id="msGallery">
	<a rel="fancybox" href="[[+url:default=`/assets/ms2gallery/minishop2/img/web/ms2_big.png`]]" target="_blank">
		<img src="[[+360x270:default=`/assets/components/ms2gallery/img/web/ms2_medium.png`]]" width="360" height="270" alt="" title="" id="mainImage" />
	</a>
	<div class="row">
		[[+rows]]
	</div>
</div>',
    ),
  ),
  '73750dfa3d1903bbab2604ab04dffc5e' => 
  array (
    'criteria' => 
    array (
      'name' => 'tpl.ms2Gallery.empty',
    ),
    'object' => 
    array (
      'id' => 25,
      'source' => 1,
      'property_preprocess' => 0,
      'name' => 'tpl.ms2Gallery.empty',
      'description' => '',
      'editor_type' => 0,
      'category' => 19,
      'cache_type' => 0,
      'snippet' => '<div id="msGallery">
	<img src="[[++assets_url]]components/ms2gallery/img/web/ms2_medium.png" width="360" height="270" alt="no photo" title="" />
</div>',
      'locked' => 0,
      'properties' => NULL,
      'static' => 0,
      'static_file' => 'core/components/ms2gallery/elements/chunks/chunk.ms2gallery_empty.tpl',
      'content' => '<div id="msGallery">
	<img src="[[++assets_url]]components/ms2gallery/img/web/ms2_medium.png" width="360" height="270" alt="no photo" title="" />
</div>',
    ),
  ),
  '54b72b2ffaab9c07c04378143102d1ce' => 
  array (
    'criteria' => 
    array (
      'name' => 'ms2Gallery',
    ),
    'object' => 
    array (
      'id' => 39,
      'source' => 1,
      'property_preprocess' => 0,
      'name' => 'ms2Gallery',
      'description' => '',
      'editor_type' => 0,
      'category' => 19,
      'cache_type' => 0,
      'snippet' => '/* @var array $scriptProperties */
/* @var ms2Gallery $ms2Gallery */
$ms2Gallery = $modx->getService(\'ms2gallery\',\'ms2Gallery\', MODX_CORE_PATH.\'components/ms2gallery/model/ms2gallery/\');
/* @var pdoFetch $pdoFetch */
if (!$modx->loadClass(\'pdofetch\', MODX_CORE_PATH . \'components/pdotools/model/pdotools/\', false, true)) {return false;}
$pdoFetch = new pdoFetch($modx, $scriptProperties);

$extensionsDir = $modx->getOption(\'extensionsDir\', $scriptProperties, \'components/ms2gallery/img/mgr/extensions/\', true);

// Register styles and scripts on frontend
$config = $ms2Gallery->makePlaceholders($ms2Gallery->config);
$css = $modx->getOption(\'frontend_css\', $scriptProperties, \'frontend_css\');
if (!empty($css) && preg_match(\'/\\.css/i\', $css)) {
	$modx->regClientCSS(str_replace($config[\'pl\'], $config[\'vl\'], $css));
}

$js = $modx->getOption(\'frontend_js\', $scriptProperties, \'frontend_js\');
if (!empty($js) && preg_match(\'/\\.js/i\', $js)) {
	$modx->regClientStartupScript(str_replace(\'		\', \'\', \'
		<script type="text/javascript">
			if(typeof jQuery == "undefined") {
				document.write("<script src=\\"\'.$ms2Gallery->config[\'jsUrl\'].\'web/lib/jquery.min.js\\" type=\\"text/javascript\\"><\\/script>");
			}
		</script>
	\'), true);
	$modx->regClientScript(str_replace($config[\'pl\'], $config[\'vl\'], $js));
}

/** @var modResource $resource */
$resource = (!empty($resource) && $resource != $modx->resource->id)
	? $modx->getObject(\'modResource\', $resource)
	: $modx->resource;

if (empty($limit) && !empty($offset)) {$scriptProperties[\'limit\'] = 10000;}
$where = array(
	\'resource_id\' => $resource->get(\'id\'),
	\'parent\' => 0,
);
if (!empty($filetype)) {
	$where[\'type:IN\'] = array_map(\'trim\', explode(\',\', $filetype));
}
if (empty($showInactive)) {
	$where[\'active\'] = 1;
}
// processing additional query params
if (!empty($scriptProperties[\'where\'])) {
	$tmp = $modx->fromJSON($scriptProperties[\'where\']);
	if (is_array($tmp) && !empty($tmp)) {
		$where = array_merge($where, $tmp);
	}
}
unset($scriptProperties[\'where\']);

// Default parameters
$default = array(
	\'class\' => \'msResourceFile\',
	\'where\' => $modx->toJSON($where),
	//\'select\' => \'{"msResourceFile":"all"}\',
	\'limit\' => $limit,
	\'sortby\' => \'rank\',
	\'sortdir\' => \'ASC\',
	\'fastMode\' => false,
	\'return\' => \'data\',
	\'nestedChunkPrefix\' => \'ms2gallery_\',
);

// Merge all properties and run!
$scriptProperties[\'tpl\'] = !empty($tplRow) ? $tplRow : \'\';
$pdoFetch->setConfig(array_merge($default, $scriptProperties));
$rows = $pdoFetch->run();

if (!empty($rows)) {
	$tmp = current($rows);
	$resolution = array();
	$ms2Gallery->initializeMediaSource($modx->context->key, $tmp[\'source\']);
	$properties = $ms2Gallery->mediaSource->getProperties();
	if (isset($properties[\'thumbnails\'][\'value\'])) {
		$fileTypes = $modx->fromJSON($properties[\'thumbnails\'][\'value\']);
		foreach ($fileTypes as $v) {
			$resolution[] = $v[\'w\'].\'x\'.$v[\'h\'];
		}
	}
}


// Processing rows
$output = null; $images = array();
$pdoFetch->addTime(\'Fetching thumbnails\');
foreach ($rows as $k => $row) {
	$row[\'idx\'] = $pdoFetch->idx++;
	$images[$row[\'id\']] = $row;

	if (isset($row[\'type\']) && $row[\'type\'] == \'image\') {
		$q = $modx->newQuery(\'msResourceFile\', array(\'parent\' => $row[\'id\']));
		$q->select(\'url\');
		if ($q->prepare() && $q->stmt->execute()) {
			while ($tmp = $q->stmt->fetch(PDO::FETCH_COLUMN)) {
				if (preg_match(\'/((?:\\d{1,4}|)x(?:\\d{1,4}|))/\', $tmp, $size)) {
					$images[$row[\'id\']][$size[0]] = $tmp;
				}
			}
		}
	}
	elseif (isset($row[\'type\'])) {
		$row[\'thumbnail\'] = $row[\'url\'] =  (file_exists(MODX_ASSETS_PATH . $extensionsDir . $row[\'type\'] . \'.png\'))
			? MODX_ASSETS_URL . $extensionsDir . $row[\'type\'].\'.png\'
			: MODX_ASSETS_URL . $extensionsDir . \'other.png\';
		foreach ($resolution as $v) {
			$images[$row[\'id\']][$v] = $row[\'thumbnail\'];
		}
	}
}

// Processing chunks
$pdoFetch->addTime(\'Processing chunks\');
$output = array();
foreach ($images as $row) {
	$tpl = $pdoFetch->defineChunk($row);

	$output[] = empty($tpl)
		? $pdoFetch->getChunk(\'\', $row)
		: $pdoFetch->getChunk($tpl, $row, $pdoFetch->config[\'fastMode\']);
}
$pdoFetch->addTime(\'Returning processed chunks\');

// Return output
$log = \'\';
if ($modx->user->hasSessionContext(\'mgr\') && !empty($showLog)) {
	$log .= \'<pre class="msGalleryLog">\' . print_r($pdoFetch->getTime(), 1) . \'</pre>\';
}

if (!empty($toSeparatePlaceholders)) {
	$output[\'log\'] = $log;
	$modx->setPlaceholders($output, $toSeparatePlaceholders);
}
else {
	if (count($output) === 1 && !empty($tplSingle)) {
		$output = $pdoFetch->getChunk($tplSingle, array_shift($images));
	}
	else {
		if (empty($outputSeparator)) {$outputSeparator = "\\n";}
		$output = implode($outputSeparator, $output);

		if (!empty($tplOuter) && !empty($output)) {
			$arr = array_shift($images);
			$arr[\'rows\'] = $output;
			$output = $pdoFetch->getChunk($tplOuter, $arr);
		}
		elseif (empty($output)) {
			$output = !empty($tplEmpty)
				? $pdoFetch->getChunk($tplEmpty)
				: \'\';
		}
	}

	$output .= $log;
	if (!empty($toPlaceholder)) {
		$modx->setPlaceholder($toPlaceholder, $output);
	}
	else {
		return $output;
	}
}',
      'locked' => 0,
      'properties' => 'a:15:{s:8:"resource";a:7:{s:4:"name";s:8:"resource";s:4:"desc";s:24:"ms2gallery_prop_resource";s:4:"type";s:11:"numberfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:7:"showLog";a:7:{s:4:"name";s:7:"showLog";s:4:"desc";s:23:"ms2gallery_prop_showLog";s:4:"type";s:13:"combo-boolean";s:7:"options";a:0:{}s:5:"value";b:0;s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:13:"toPlaceholder";a:7:{s:4:"name";s:13:"toPlaceholder";s:4:"desc";s:29:"ms2gallery_prop_toPlaceholder";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:6:"tplRow";a:7:{s:4:"name";s:6:"tplRow";s:4:"desc";s:22:"ms2gallery_prop_tplRow";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:18:"tpl.ms2Gallery.row";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:8:"tplOuter";a:7:{s:4:"name";s:8:"tplOuter";s:4:"desc";s:24:"ms2gallery_prop_tplOuter";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:20:"tpl.ms2Gallery.outer";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:8:"tplEmpty";a:7:{s:4:"name";s:8:"tplEmpty";s:4:"desc";s:24:"ms2gallery_prop_tplEmpty";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:20:"tpl.ms2Gallery.empty";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:5:"limit";a:7:{s:4:"name";s:5:"limit";s:4:"desc";s:21:"ms2gallery_prop_limit";s:4:"type";s:11:"numberfield";s:7:"options";a:0:{}s:5:"value";i:0;s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:6:"offset";a:7:{s:4:"name";s:6:"offset";s:4:"desc";s:22:"ms2gallery_prop_offset";s:4:"type";s:11:"numberfield";s:7:"options";a:0:{}s:5:"value";i:0;s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:5:"where";a:7:{s:4:"name";s:5:"where";s:4:"desc";s:21:"ms2gallery_prop_where";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:8:"filetype";a:7:{s:4:"name";s:8:"filetype";s:4:"desc";s:24:"ms2gallery_prop_filetype";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:12:"showInactive";a:7:{s:4:"name";s:12:"showInactive";s:4:"desc";s:28:"ms2gallery_prop_showInactive";s:4:"type";s:13:"combo-boolean";s:7:"options";a:0:{}s:5:"value";b:0;s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:6:"sortby";a:7:{s:4:"name";s:6:"sortby";s:4:"desc";s:22:"ms2gallery_prop_sortby";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:4:"rank";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:7:"sortdir";a:7:{s:4:"name";s:7:"sortdir";s:4:"desc";s:23:"ms2gallery_prop_sortdir";s:4:"type";s:4:"list";s:7:"options";a:2:{i:0;a:2:{s:4:"text";s:3:"ASC";s:5:"value";s:3:"ASC";}i:1;a:2:{s:4:"text";s:4:"DESC";s:5:"value";s:4:"DESC";}}s:5:"value";s:3:"ASC";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:12:"frontend_css";a:7:{s:4:"name";s:12:"frontend_css";s:4:"desc";s:28:"ms2gallery_prop_frontend_css";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:26:"[[+cssUrl]]web/default.css";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}s:11:"frontend_js";a:7:{s:4:"name";s:11:"frontend_js";s:4:"desc";s:27:"ms2gallery_prop_frontend_js";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:24:"[[+jsUrl]]web/default.js";s:7:"lexicon";s:21:"ms2gallery:properties";s:4:"area";s:0:"";}}',
      'moduleguid' => '',
      'static' => 0,
      'static_file' => 'core/components/ms2gallery/elements/snippets/snippet.ms2gallery.php',
      'content' => '/* @var array $scriptProperties */
/* @var ms2Gallery $ms2Gallery */
$ms2Gallery = $modx->getService(\'ms2gallery\',\'ms2Gallery\', MODX_CORE_PATH.\'components/ms2gallery/model/ms2gallery/\');
/* @var pdoFetch $pdoFetch */
if (!$modx->loadClass(\'pdofetch\', MODX_CORE_PATH . \'components/pdotools/model/pdotools/\', false, true)) {return false;}
$pdoFetch = new pdoFetch($modx, $scriptProperties);

$extensionsDir = $modx->getOption(\'extensionsDir\', $scriptProperties, \'components/ms2gallery/img/mgr/extensions/\', true);

// Register styles and scripts on frontend
$config = $ms2Gallery->makePlaceholders($ms2Gallery->config);
$css = $modx->getOption(\'frontend_css\', $scriptProperties, \'frontend_css\');
if (!empty($css) && preg_match(\'/\\.css/i\', $css)) {
	$modx->regClientCSS(str_replace($config[\'pl\'], $config[\'vl\'], $css));
}

$js = $modx->getOption(\'frontend_js\', $scriptProperties, \'frontend_js\');
if (!empty($js) && preg_match(\'/\\.js/i\', $js)) {
	$modx->regClientStartupScript(str_replace(\'		\', \'\', \'
		<script type="text/javascript">
			if(typeof jQuery == "undefined") {
				document.write("<script src=\\"\'.$ms2Gallery->config[\'jsUrl\'].\'web/lib/jquery.min.js\\" type=\\"text/javascript\\"><\\/script>");
			}
		</script>
	\'), true);
	$modx->regClientScript(str_replace($config[\'pl\'], $config[\'vl\'], $js));
}

/** @var modResource $resource */
$resource = (!empty($resource) && $resource != $modx->resource->id)
	? $modx->getObject(\'modResource\', $resource)
	: $modx->resource;

if (empty($limit) && !empty($offset)) {$scriptProperties[\'limit\'] = 10000;}
$where = array(
	\'resource_id\' => $resource->get(\'id\'),
	\'parent\' => 0,
);
if (!empty($filetype)) {
	$where[\'type:IN\'] = array_map(\'trim\', explode(\',\', $filetype));
}
if (empty($showInactive)) {
	$where[\'active\'] = 1;
}
// processing additional query params
if (!empty($scriptProperties[\'where\'])) {
	$tmp = $modx->fromJSON($scriptProperties[\'where\']);
	if (is_array($tmp) && !empty($tmp)) {
		$where = array_merge($where, $tmp);
	}
}
unset($scriptProperties[\'where\']);

// Default parameters
$default = array(
	\'class\' => \'msResourceFile\',
	\'where\' => $modx->toJSON($where),
	//\'select\' => \'{"msResourceFile":"all"}\',
	\'limit\' => $limit,
	\'sortby\' => \'rank\',
	\'sortdir\' => \'ASC\',
	\'fastMode\' => false,
	\'return\' => \'data\',
	\'nestedChunkPrefix\' => \'ms2gallery_\',
);

// Merge all properties and run!
$scriptProperties[\'tpl\'] = !empty($tplRow) ? $tplRow : \'\';
$pdoFetch->setConfig(array_merge($default, $scriptProperties));
$rows = $pdoFetch->run();

if (!empty($rows)) {
	$tmp = current($rows);
	$resolution = array();
	$ms2Gallery->initializeMediaSource($modx->context->key, $tmp[\'source\']);
	$properties = $ms2Gallery->mediaSource->getProperties();
	if (isset($properties[\'thumbnails\'][\'value\'])) {
		$fileTypes = $modx->fromJSON($properties[\'thumbnails\'][\'value\']);
		foreach ($fileTypes as $v) {
			$resolution[] = $v[\'w\'].\'x\'.$v[\'h\'];
		}
	}
}


// Processing rows
$output = null; $images = array();
$pdoFetch->addTime(\'Fetching thumbnails\');
foreach ($rows as $k => $row) {
	$row[\'idx\'] = $pdoFetch->idx++;
	$images[$row[\'id\']] = $row;

	if (isset($row[\'type\']) && $row[\'type\'] == \'image\') {
		$q = $modx->newQuery(\'msResourceFile\', array(\'parent\' => $row[\'id\']));
		$q->select(\'url\');
		if ($q->prepare() && $q->stmt->execute()) {
			while ($tmp = $q->stmt->fetch(PDO::FETCH_COLUMN)) {
				if (preg_match(\'/((?:\\d{1,4}|)x(?:\\d{1,4}|))/\', $tmp, $size)) {
					$images[$row[\'id\']][$size[0]] = $tmp;
				}
			}
		}
	}
	elseif (isset($row[\'type\'])) {
		$row[\'thumbnail\'] = $row[\'url\'] =  (file_exists(MODX_ASSETS_PATH . $extensionsDir . $row[\'type\'] . \'.png\'))
			? MODX_ASSETS_URL . $extensionsDir . $row[\'type\'].\'.png\'
			: MODX_ASSETS_URL . $extensionsDir . \'other.png\';
		foreach ($resolution as $v) {
			$images[$row[\'id\']][$v] = $row[\'thumbnail\'];
		}
	}
}

// Processing chunks
$pdoFetch->addTime(\'Processing chunks\');
$output = array();
foreach ($images as $row) {
	$tpl = $pdoFetch->defineChunk($row);

	$output[] = empty($tpl)
		? $pdoFetch->getChunk(\'\', $row)
		: $pdoFetch->getChunk($tpl, $row, $pdoFetch->config[\'fastMode\']);
}
$pdoFetch->addTime(\'Returning processed chunks\');

// Return output
$log = \'\';
if ($modx->user->hasSessionContext(\'mgr\') && !empty($showLog)) {
	$log .= \'<pre class="msGalleryLog">\' . print_r($pdoFetch->getTime(), 1) . \'</pre>\';
}

if (!empty($toSeparatePlaceholders)) {
	$output[\'log\'] = $log;
	$modx->setPlaceholders($output, $toSeparatePlaceholders);
}
else {
	if (count($output) === 1 && !empty($tplSingle)) {
		$output = $pdoFetch->getChunk($tplSingle, array_shift($images));
	}
	else {
		if (empty($outputSeparator)) {$outputSeparator = "\\n";}
		$output = implode($outputSeparator, $output);

		if (!empty($tplOuter) && !empty($output)) {
			$arr = array_shift($images);
			$arr[\'rows\'] = $output;
			$output = $pdoFetch->getChunk($tplOuter, $arr);
		}
		elseif (empty($output)) {
			$output = !empty($tplEmpty)
				? $pdoFetch->getChunk($tplEmpty)
				: \'\';
		}
	}

	$output .= $log;
	if (!empty($toPlaceholder)) {
		$modx->setPlaceholder($toPlaceholder, $output);
	}
	else {
		return $output;
	}
}',
    ),
  ),
  '8d7e5cfcf7ba4a4be0dc2d4bf15f9530' => 
  array (
    'criteria' => 
    array (
      'name' => 'ms2Gallery',
    ),
    'object' => 
    array (
      'id' => 16,
      'source' => 1,
      'property_preprocess' => 0,
      'name' => 'ms2Gallery',
      'description' => 'Main plugin for ms2Gallery',
      'editor_type' => 0,
      'category' => 19,
      'cache_type' => 0,
      'plugincode' => '$corePath = $modx->getOption(\'core_path\', null, MODX_CORE_PATH).\'components/ms2gallery/\';

switch ($modx->event->name) {
	case \'OnDocFormRender\':
		/** @var modResource $resource */
		if ($resource instanceof msProduct || $mode == \'new\') {
			return;
		}
		$modx23 = !empty($modx->version) && version_compare($modx->version[\'full_version\'], \'2.3.0\', \'>=\');
		$modx->controller->addHtml(\'<script type="text/javascript">
			Ext.onReady(function() {
				MODx.modx23 = \'.(int)$modx23.\';
			});
		</script>\');

		/** @var ms2Gallery $ms2Gallery */
		$ms2Gallery = $modx->getService(\'ms2gallery\',\'ms2Gallery\', MODX_CORE_PATH.\'components/ms2gallery/model/ms2gallery/\');
		$modx->controller->addLexiconTopic(\'ms2gallery:default\');
		$url = $ms2Gallery->config[\'assetsUrl\'];

		$modx->controller->addJavascript($url . \'js/mgr/ms2gallery.js\');
		$modx->controller->addLastJavascript($url . \'js/mgr/misc/ms2.combo.js\');
		$modx->controller->addLastJavascript($url . \'js/mgr/misc/ms2.utils.js\');
		$modx->controller->addLastJavascript($url . \'js/mgr/misc/plupload/plupload.full.js\');
		$modx->controller->addLastJavascript($url . \'js/mgr/misc/ext.ddview.js\');
		$modx->controller->addLastJavascript($url . \'js/mgr/uploader.grid.js\');
		$modx->controller->addLastJavascript($url . \'js/mgr/gallery.view.js\');
		$modx->controller->addLastJavascript($url . \'js/mgr/gallery.window.js\');
		$modx->controller->addLastJavascript($url . \'js/mgr/gallery.panel.js\');
		$modx->controller->addCss($url . \'css/mgr/main.css\');
		if (!$modx23) {
			$modx->controller->addCss($url . \'css/mgr/font-awesome.min.css\');
		}

		$properties = $resource->getProperties(\'ms2gallery\');
		if (empty($properties[\'media_source\'])) {
			if (!$source_id = $resource->getTVValue(\'ms2Gallery\')) {
				$source_id = $properties[\'media_source\'];
			}
			$resource->setProperties(array(\'media_source\' => $source_id), \'ms2gallery\');
			$resource->save();
		}
		else {
			$source_id = $properties[\'media_source\'];
		}

		if (empty($source_id)) {
			$source_id = $modx->getOption(\'ms2gallery_source_default\');
		}
		$source_config = array();
		/** @var modMediaSource $source */
		if ($source = $modx->getObject(\'modMediaSource\', $source_id)) {
			$tmp = $source->getProperties();
			$properties = array();
			foreach ($tmp as $v) {
				$source_config[$v[\'name\']] = $v[\'value\'];
			}
		}

		if ($modx->getCount(\'modPlugin\', array(\'name\' => \'AjaxManager\', \'disabled\' => false))) {
			$modx->controller->addHtml(\'
			<script type="text/javascript">
				ms2Gallery.config = \' . $modx->toJSON($ms2Gallery->config) . \';
				ms2Gallery.config.media_source = \' . $modx->toJSON($source_config) . \';
				Ext.onReady(function() {
					window.setTimeout(function() {
						var tabs = Ext.getCmp("modx-resource-tabs");
						if (tabs) {
							tabs.add({
								xtype: "ms2gallery-page",
								id: "ms2gallery-page",
								title: _("ms2gallery"),
								record: {
									id: \' . $resource->get(\'id\') . \'
									,source: \' . $source_id . \'
								},
							});
						}
					}, 10);
				});
			</script>\');
		}
		else {
			$modx->controller->addHtml(\'
			<script type="text/javascript">
				ms2Gallery.config = \' . $modx->toJSON($ms2Gallery->config) . \';
				ms2Gallery.config.media_source = \' . $modx->toJSON($source_config) . \';
				Ext.ComponentMgr.onAvailable("modx-resource-tabs", function() {
					this.on("beforerender", function() {
						this.add({
							xtype: "ms2gallery-page",
							id: "ms2gallery-page",
							title: _("ms2gallery"),
							record: {
								id: \' . $resource->get(\'id\') . \'
								,source: \' . $source_id . \'
							},
						});
					});
					Ext.apply(this, {
							stateful: true,
							stateId: "modx-resource-tabs-state",
							stateEvents: ["tabchange"],
							getState: function() {return {activeTab:this.items.indexOf(this.getActiveTab())};
						}
					});
				});
			</script>\');
		}
		break;

	case \'OnLoadWebDocument\':
		$tstart = microtime(true);
		/** @var pdoFetch $pdoFetch */
		if (!$modx->getOption(\'ms2gallery_set_placeholders\', null, false, true) || !$pdoFetch = $modx->getService(\'pdoFetch\')) {return;}
		$plPrefix = $modx->getOption(\'ms2gallery_placeholders_prefix\', null, \'ms2g\', true);

		$options = array(\'loadModels\' => \'ms2gallery\');
		$where = array(\'resource_id\' => $modx->resource->id, \'parent\' => 0);

		$parents = $pdoFetch->getCollection(\'msResourceFile\', $where, $options);
		$options[\'select\'] = \'url\';
		foreach ($parents as &$parent) {
			$where = array(\'parent\' => $parent[\'id\']);
			if ($children = $pdoFetch->getCollection(\'msResourceFile\', $where, $options)) {
				foreach ($children as $child) {
					if (preg_match(\'/((?:\\d{1,4}|)x(?:\\d{1,4}|))/\', $child[\'url\'], $size)) {
						$parent[$size[0]] = $child[\'url\'];
					}
				}
			}
			$pls = $pdoFetch->makePlaceholders($parent, $plPrefix . $parent[\'rank\'] . \'.\', \'[[+\', \']]\', false);
			$pls[\'vl\'][$plPrefix . $parent[\'rank\']] = htmlentities(print_r($parent, 1), ENT_QUOTES, \'UTF-8\');
			$modx->setPlaceholders($pls[\'vl\']);
		}

		$modx->log(modX::LOG_LEVEL_INFO, \'[ms2Gallery] Set image placeholders for page id = \' . $modx->resource->id .\' in \' . number_format(microtime(true) - $tstart, 7) . \' sec.\');
		break;
}',
      'locked' => 0,
      'properties' => NULL,
      'disabled' => 0,
      'moduleguid' => '',
      'static' => 1,
      'static_file' => 'core/components/ms2gallery/elements/plugins/plugin.ms2gallery.php',
      'content' => '$corePath = $modx->getOption(\'core_path\', null, MODX_CORE_PATH).\'components/ms2gallery/\';

switch ($modx->event->name) {
	case \'OnDocFormRender\':
		/** @var modResource $resource */
		if ($resource instanceof msProduct || $mode == \'new\') {
			return;
		}
		$modx23 = !empty($modx->version) && version_compare($modx->version[\'full_version\'], \'2.3.0\', \'>=\');
		$modx->controller->addHtml(\'<script type="text/javascript">
			Ext.onReady(function() {
				MODx.modx23 = \'.(int)$modx23.\';
			});
		</script>\');

		/** @var ms2Gallery $ms2Gallery */
		$ms2Gallery = $modx->getService(\'ms2gallery\',\'ms2Gallery\', MODX_CORE_PATH.\'components/ms2gallery/model/ms2gallery/\');
		$modx->controller->addLexiconTopic(\'ms2gallery:default\');
		$url = $ms2Gallery->config[\'assetsUrl\'];

		$modx->controller->addJavascript($url . \'js/mgr/ms2gallery.js\');
		$modx->controller->addLastJavascript($url . \'js/mgr/misc/ms2.combo.js\');
		$modx->controller->addLastJavascript($url . \'js/mgr/misc/ms2.utils.js\');
		$modx->controller->addLastJavascript($url . \'js/mgr/misc/plupload/plupload.full.js\');
		$modx->controller->addLastJavascript($url . \'js/mgr/misc/ext.ddview.js\');
		$modx->controller->addLastJavascript($url . \'js/mgr/uploader.grid.js\');
		$modx->controller->addLastJavascript($url . \'js/mgr/gallery.view.js\');
		$modx->controller->addLastJavascript($url . \'js/mgr/gallery.window.js\');
		$modx->controller->addLastJavascript($url . \'js/mgr/gallery.panel.js\');
		$modx->controller->addCss($url . \'css/mgr/main.css\');
		if (!$modx23) {
			$modx->controller->addCss($url . \'css/mgr/font-awesome.min.css\');
		}

		$properties = $resource->getProperties(\'ms2gallery\');
		if (empty($properties[\'media_source\'])) {
			if (!$source_id = $resource->getTVValue(\'ms2Gallery\')) {
				$source_id = $properties[\'media_source\'];
			}
			$resource->setProperties(array(\'media_source\' => $source_id), \'ms2gallery\');
			$resource->save();
		}
		else {
			$source_id = $properties[\'media_source\'];
		}

		if (empty($source_id)) {
			$source_id = $modx->getOption(\'ms2gallery_source_default\');
		}
		$source_config = array();
		/** @var modMediaSource $source */
		if ($source = $modx->getObject(\'modMediaSource\', $source_id)) {
			$tmp = $source->getProperties();
			$properties = array();
			foreach ($tmp as $v) {
				$source_config[$v[\'name\']] = $v[\'value\'];
			}
		}

		if ($modx->getCount(\'modPlugin\', array(\'name\' => \'AjaxManager\', \'disabled\' => false))) {
			$modx->controller->addHtml(\'
			<script type="text/javascript">
				ms2Gallery.config = \' . $modx->toJSON($ms2Gallery->config) . \';
				ms2Gallery.config.media_source = \' . $modx->toJSON($source_config) . \';
				Ext.onReady(function() {
					window.setTimeout(function() {
						var tabs = Ext.getCmp("modx-resource-tabs");
						if (tabs) {
							tabs.add({
								xtype: "ms2gallery-page",
								id: "ms2gallery-page",
								title: _("ms2gallery"),
								record: {
									id: \' . $resource->get(\'id\') . \'
									,source: \' . $source_id . \'
								},
							});
						}
					}, 10);
				});
			</script>\');
		}
		else {
			$modx->controller->addHtml(\'
			<script type="text/javascript">
				ms2Gallery.config = \' . $modx->toJSON($ms2Gallery->config) . \';
				ms2Gallery.config.media_source = \' . $modx->toJSON($source_config) . \';
				Ext.ComponentMgr.onAvailable("modx-resource-tabs", function() {
					this.on("beforerender", function() {
						this.add({
							xtype: "ms2gallery-page",
							id: "ms2gallery-page",
							title: _("ms2gallery"),
							record: {
								id: \' . $resource->get(\'id\') . \'
								,source: \' . $source_id . \'
							},
						});
					});
					Ext.apply(this, {
							stateful: true,
							stateId: "modx-resource-tabs-state",
							stateEvents: ["tabchange"],
							getState: function() {return {activeTab:this.items.indexOf(this.getActiveTab())};
						}
					});
				});
			</script>\');
		}
		break;

	case \'OnLoadWebDocument\':
		$tstart = microtime(true);
		/** @var pdoFetch $pdoFetch */
		if (!$modx->getOption(\'ms2gallery_set_placeholders\', null, false, true) || !$pdoFetch = $modx->getService(\'pdoFetch\')) {return;}
		$plPrefix = $modx->getOption(\'ms2gallery_placeholders_prefix\', null, \'ms2g\', true);

		$options = array(\'loadModels\' => \'ms2gallery\');
		$where = array(\'resource_id\' => $modx->resource->id, \'parent\' => 0);

		$parents = $pdoFetch->getCollection(\'msResourceFile\', $where, $options);
		$options[\'select\'] = \'url\';
		foreach ($parents as &$parent) {
			$where = array(\'parent\' => $parent[\'id\']);
			if ($children = $pdoFetch->getCollection(\'msResourceFile\', $where, $options)) {
				foreach ($children as $child) {
					if (preg_match(\'/((?:\\d{1,4}|)x(?:\\d{1,4}|))/\', $child[\'url\'], $size)) {
						$parent[$size[0]] = $child[\'url\'];
					}
				}
			}
			$pls = $pdoFetch->makePlaceholders($parent, $plPrefix . $parent[\'rank\'] . \'.\', \'[[+\', \']]\', false);
			$pls[\'vl\'][$plPrefix . $parent[\'rank\']] = htmlentities(print_r($parent, 1), ENT_QUOTES, \'UTF-8\');
			$modx->setPlaceholders($pls[\'vl\']);
		}

		$modx->log(modX::LOG_LEVEL_INFO, \'[ms2Gallery] Set image placeholders for page id = \' . $modx->resource->id .\' in \' . number_format(microtime(true) - $tstart, 7) . \' sec.\');
		break;
}',
    ),
  ),
  '1d77c48bd99b5e8db8908f212fda12c8' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 16,
      'event' => 'OnDocFormRender',
    ),
    'object' => 
    array (
      'pluginid' => 16,
      'event' => 'OnDocFormRender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  'dea14f2bda0da3befca0731ffe04de47' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 16,
      'event' => 'OnLoadWebDocument',
    ),
    'object' => 
    array (
      'pluginid' => 16,
      'event' => 'OnLoadWebDocument',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
);