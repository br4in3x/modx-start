<?php

class googleMapSetting {
	
	function __construct(xPDO &$modx) {
		/** @var modX $modx */
		$this->modx = $modx;
		$this->setting = $this->modx->getObject('modSystemSetting', 'latlng');
	}
	
	/**
	 * Makes a request to Google geocoding service
	 * @param string $address
	 * @return JSON
	 */
	public function geoCode($address) {
		
		$geocode = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=".$address."&sensor=false");			
		$output = json_decode($geocode);
		
		if (count($output)) $this->modx->log(MODX_LOG_LEVEL_INFO,'googleMapSetting: Got response.');
		
		$latlng = [
			'lat' => $output->results[0]->geometry->location->lat,
			'lng' => $output->results[0]->geometry->location->lng
		];
		
		return json_encode($latlng);
		
	}

	public function run( $address, $check = false ) {

		// nothing to do?
		if ($check && $this->setting) return;

		$latlng = $this->geoCode( $address );
		$this->save( $latlng );
	}

	public function save($latlng) {

		$setting = $this->setting;

		if ( $this->setting ) {
			$setting->set('value', $latlng);
		} else {
			$setting = $this->modx->newObject('modSystemSetting');
			$setting->set('key','latlng');
		}
		return $setting->save();
	}
}